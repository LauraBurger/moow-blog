import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    dark: true,
    options: { customProperties: true },
    themes: {
      dark: {
        'orange-dark': '#d77c30',
        'orange-medium': '#fa9f42',
        'orange-light': '#fbbe70',
        'strawberry-dark': '#ff4460',
        'strawberry-light': '#ff7c7a',
        'blueberry-dark': '#3ec1f9',
        'blueberry-light': '#6ddafb',
        'apple-dark': '#6de079',
        'apple-light': '#91ec92',
        'lemon-dark': '#fcc93f',
        'lemon-light': '#fdda6e',
        'black-dark': '#202327',
        'black-medium': '#272a30',
        'black-light': '#2e3238',
        'grey-dark': '#5c6470',
        'grey-medium': '#9fa2a8',
        'grey-light': '#d8dadf',
        'white-medium': '#e9ebed',
        'white-light': '#ffffff'
      }
    }
  }
})
