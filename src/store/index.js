import Vuex from 'vuex'
import faker from '@faker-js/faker'

const IMAGE_API = 'https://source.unsplash.com/1600x900/'
const DEFAULT_IMAGE = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUZskPuCthi2HyWD2pzqUatw_RluPi5gkAXw&usqp=CAU'
const ARTICLES_NUMBER = 50

const categories = ['fashion', 'food', 'travel', 'sports', 'nature', 'games', 'technology', 'music', 'movie', 'lifestyle', 'diy']
const tags = ['#dress', '#green', '#silk', '#denim', '#blue', '#slim']

function generateArticles (number) {
  const subjects = Array.from({ length: number }, () => categories[Math.floor(Math.random() * categories.length)])

  return subjects.map(subject =>
    ({
      icon: DEFAULT_IMAGE,
      image: IMAGE_API + `?${subject}`,
      category: subject,
      title: faker.lorem.words(),
      text: faker.lorem.paragraphs(),
      tags: tags.slice(0, Math.random() * tags.length)
    })
  )
}

export default new Vuex.Store({
  state: {
    categories: categories.map(categorie =>
      ({
        src: DEFAULT_IMAGE,
        title: categorie,
        landscape: IMAGE_API + `?${categorie}`
      })
    ),
    articles: generateArticles(ARTICLES_NUMBER),
    tags: tags.map(tag => ({
      tag,
      src: `https://source.unsplash.com/1600x900/?${tag.substring(1)}`
    }))
  },
  getters: {
    tags (state) {
      let allTags = []
      const articles = state.articles
      for (let i = 0; i < articles.length; i++) {
        const currentTags = articles[i].tags
        allTags = allTags.concat(currentTags)
      }
      return allTags
    },
    countTag: (state, getters) => (tag) => {
      let counter = 0
      const tags = getters.tags
      for (let i = 0; i < tags.length; i++) {
        const currentTag = tags[i]
        if (currentTag === tag) {
          counter += 1
        }
      }
      return counter
    },
    tagsOccurence (state, getters) {
      const result = {}
      const tags = getters.tags
      for (let i = 0; i < tags.length; i++) {
        const tag = tags[i]
        result[tag] = getters.countTag(tag)
      }
      return result
    },
    getTagImage: (state) => (tag) => {
      const tags = state.tags
      const found = tags.find(currentTag => currentTag.tag === tag)
      return found ? found.src : ''
    }
  }
})
