import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import Vuex from './plugins/vuex'
import store from './store/index'
import router from './router'

Vue.config.productionTip = false

new Vue({
  vuetify,
  Vuex,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
