import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/pages/Home.vue'
import Categories from '@/pages/Categories.vue'
import Articles from '@/pages/Articles.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/categories',
    name: 'categories',
    component: Categories
  },
  {
    path: '/:page?',
    name: 'home',
    component: Home,
    props: route => ({
      page: parseInt(route.params.page || 1),
      nbArticlesPerPage: 2
    })
  },
  {
    path: '/articles/:category/:page?',
    name: 'articles',
    component: Articles,
    props: route => ({
      category: route.params.category,
      page: parseInt(route.params.page || 1),
      nbArticlesPerPage: 2
    })
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
